#ifndef sensor_bus_H
#define sensor_bus_H
/*
    Configuration struct.
    The implementation assumes that all sensors are attached to the same i2C bus as happens on the TI sensor hub
 */
typedef struct {
    uint_fast8_t throughput;
    uint_fast8_t role;
    } sensorBusConfig;

typedef struct{
    uint_fast8_t  *data;
    uint_fast8_t  size;
    uint_fast32_t address
} dataPacket_t
    
void sensorBusInit(sensorBusConfig_t *config);

void sensorBusSet(sensorBusConfig_t *config);

void sensorBusPowerOff();

void sensorBusPowerOn();

void sensorBusSendData(dataPacket_t *packet);

void sensorBusGet(dataPacket_t *packet);

#endif // sensor_bus_H
