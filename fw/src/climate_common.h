#ifndef climate_common_H
#define climate_common_H

#include <stdint.h>

typedef struct {
    uint_fast8_t humidity;
} climate;

#endif // climate_common_H
