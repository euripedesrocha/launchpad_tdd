## Launchpad TDD 

Este projeto objetiva testar as capacidades do ceedling como build system e o Unity/CMock como bibliotecas de suporte ao desenvolvimento de sistemas embarcados
utilizando TDD. Para teste será utilizada uma launchpad tiva-c com o sensor hub booster pack

[Wiki](https://bitbucket.org/euripedesrocha/launchpad_tdd/wiki/Home)

